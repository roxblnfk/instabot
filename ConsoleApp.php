<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 12.10.2018
 * Time: 14:24
 */

namespace roxblnfk\instabot;

use roxblnfk\instabot\models\App;
use roxblnfk\instabot\models\ioHelper;

class ConsoleApp {
    static $outCharset = 'cp866';
    /**
     * @param array $argv
     * @throws \Exception
     */
    public static function run($argv) {
        # Config
        ioHelper::$outCharset = static::$outCharset;
        try {
            $globals = App::parseConsoleCommands($argv);
            // ioHelper::print_r($globals);
            $appProps = array_shift($globals);
            # Help
            if (!$appProps || isset($appProps['help'])) {
                ioHelper::$debugLevel = 5;
                ioHelper::$logLevel = 0;
                App::help($globals);
                App::exit();
            }

            App::init($appProps);

            # run scenarios
            foreach ($globals as $scenarioName => $params) {
                $scenario = App::loadScenario($scenarioName, $params);
                $scenario->run();
            }
            App::exit();
        } catch (\Exception $e) {
            ioHelper::ioErr($e, App::DEBUG_LEVEL_EXCEPTION);
        }
    }
}
