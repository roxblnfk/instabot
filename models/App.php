<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 19.06.2018
 * Time: 16:16
 */

namespace roxblnfk\instabot\models;


use InstagramAPI\Exception\ChallengeRequiredException;
use InstagramAPI\Response\LoginResponse;
use roxblnfk\instabot\models\InstagramExtended as Instagram;
use roxblnfk\instabot\scenario\baseScenario;
use Kint;

class App {

    public static $isDebug = false;
    public static $isTest = true;
    /** @var Instagram */
    public static $instance;
    /** @var string|null */
    public static $sessionDir;
    /** @var bool User logged in */
    private static $loggedIn = false;
    /** @var string[] */
    private static $user = [
        'login'    => null,
        'password' => null,
        'code'     => null,
    ];
    /** @var bool */
    public static $modeHelp = false;
    private static $scenarioList = [
        'uploadPhoto',
        'getUsersFeed',
    ];
    private static $shortCommands = [
        'l'    => 'login',
        'p'    => 'password',
        's'    => 'scenario',
        't'    => 'test',
        'd'    => 'debug',
        'log'  => 'log-level',
    ];
    const
        DEBUG_LEVEL_ALL = 5,
        // DEBUG_LEVEL_ALL = 4,
        DEBUG_LEVEL_ANY_DEBUG = 3,
        DEBUG_LEVEL_MESSAGES = 2,
        DEBUG_LEVEL_EXCEPTION = 1,
        DEBUG_LEVEL_OFF = 0;

    /**
     * @param array $params
     * @throws \Exception
     */
    public static function init($params) {
        static::$instance = new Instagram(static::$isDebug > 0, false, [
            'storage'    => 'file',
            'basefolder' => static::$sessionDir ?? getcwd() . '/sessions/',
        ]);
        $fn_set = function (&$var, $name, $callback = null) use (&$params) {
            if (key_exists($name, $params))
                $var = is_callable($callback) ? $callback($params[$name]) : $params[$name];
        };

        # Debug mode
        $debug = ioHelper::$debugLevel;
        $fn_set($debug, 'debug', 'intval');
        static::setDebug($debug);
        $logLevel = ioHelper::$logLevel;
        $fn_set($logLevel, 'log-level', 'intval');
        ioHelper::$logLevel = $logLevel;
        # Test mode
        $fn_set(static::$isTest, 'test', function ($v) { return filter_var($v, FILTER_VALIDATE_BOOLEAN); });

        $fn_set(static::$user['login'], 'login', 'trim');
        $fn_set(static::$user['password'], 'password', 'trim');
        $fn_set(static::$user['code'], 'code', 'trim');

        if (!strlen(static::$user['login']))
            throw new \Exception('Login not specified!', 1001);
        if (!strlen(static::$user['password']))
            throw new \Exception('Password not specified!', 1002);

        ioHelper::line(sprintf(
            "User: %s:%s - %s",
            static::$user['login'],
            static::$user['password'],
            static::$user['code']
        ), self::DEBUG_LEVEL_ALL);

        static::login(static::$user['login'], static::$user['password']);
    }
    /**
     * @param string $username
     * @param string $password
     * @throws \Exception
     */
    public static function login(string $username, string $password, $retry = 2) {
        if ($retry <= 0) {
            throw new \Exception('Too many login actions', 1008);
        }
        if (static::$loggedIn || APP::$isTest)
            return;
        try {
            $loginResponse = static::$instance->login($username, $password);
            # two factor login
            if ($loginResponse !== null && $loginResponse->isTwoFactorRequired()) {
                $twoFactorIdentifier = $loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();
                // The "STDIN" lets you paste the code via terminal for testing.
                // You should replace this line with the logic you want.
                // The verification code will be sent by Instagram via SMS.
                ioHelper::message("SMS code required!", 1001);
                $verificationCode = ioHelper::ioRead();
                ioHelper::line("Given code: {$verificationCode}", App::DEBUG_LEVEL_ANY_DEBUG);
                static::$user['code'] = $verificationCode;
                static::$instance->finishTwoFactorLogin($username, $password, $twoFactorIdentifier, $verificationCode);
            }
        } catch (\Exception $e) {
            /** @var LoginResponse $response */
            if (!method_exists($e, 'getResponse' ))
                throw new \Exception($e->getMessage(), 1004); # !??!

            if ($e instanceof ChallengeRequiredException
                // && $response->getErrorType() === 'checkpoint_challenge_required'
            ) {
                if ($retry === 1) {
                    throw new \Exception('Too many login actions');
                }
                // throw new \Exception($e->getMessage(), 1005);
                ioHelper::line("Challenge required!", App::DEBUG_LEVEL_ANY_DEBUG);
                /** @var LoginResponse $response */
                $response = $e->getResponse();

                // $requestStr = substr($resp->getChallenge()->getApiPath(), 1);
                $requestStr = ltrim($response->getChallenge()->getApiPath(), '/');

                ioHelper::line("Request str: {$requestStr}", App::DEBUG_LEVEL_ANY_DEBUG);
                # wait
                sleep(rand(4, 6));

                $customResponse = static::$instance
                    ->request($requestStr)
                    ->setNeedsAuth(false)
                    ->addPost("choice", 0) # verify method: 0 - sms, 1 - email

                    ->addPost('_uuid',      static::$instance->uuid)
                    ->addPost('guid',       static::$instance->uuid)
                    ->addPost('device_id',  static::$instance->device_id)
                    // ->addPost('_uid',       static::$instance->account_id)
                    ->addPost('_csrftoken', static::$instance->client->getToken())


                    ->getDecodedResponse();

                if (!is_array($customResponse)) {
                    ioHelper::line("Weird response from challenge request...", App::DEBUG_LEVEL_ANY_DEBUG);
                    ioHelper::print_r($customResponse, App::DEBUG_LEVEL_ANY_DEBUG);
                    throw new \Exception('Weird response from challenge request...', 1005);
                }
                /* $customResponse
                Array
                (
                    [step_name] => verify_code
                    [step_data] => Array
                        (
                            [security_code] => None
                            [sms_resend_delay] => 60
                            [phone_number_preview] => 7-60
                            [resend_delay] => 60
                            [contact_point] => 7-60
                            [form_type] => phone_number
                            [phone_number_formatted] => +7 *** ***-77-60
                            [phone_number] => +7 *** ***-77-60
                        )

                    [user_id] => 8052113864
                    [nonce_code] => 7wd2oTvM6x
                    [status] => ok
                )
                */
                if (
                    $customResponse['status'] === 'ok'
                    && isset($customResponse['action'])
                    && $customResponse['action'] === 'close'
                ) {
                    ioHelper::line("Checkpoint bypassed", App::DEBUG_LEVEL_ANY_DEBUG);
                    throw new \Exception('Checkpoint bypassed', 1005);
                }

                $userId = $customResponse['user_id'];
                $ChallengeId = $customResponse['nonce_code'];

                try {
                    ioHelper::message("Challenge code required!", 1005);
                    $challengeCode = ioHelper::ioRead();

                    static::$instance->changeUser($username, $password);
                    // static::$instance->account_id = $userId;

                    $requestWithCode = static::$instance
                        ->request("challenge/{$userId}/{$ChallengeId}/")
                        ->setNeedsAuth(false)
                        ->addPost("security_code", $challengeCode)

                        ->addPost('_uuid',      static::$instance->uuid)
                        ->addPost('guid',       static::$instance->uuid)
                        ->addPost('device_id',  static::$instance->device_id)
                        // ->addPost('_uid',       static::$instance->account_id)
                        ->addPost('_csrftoken', static::$instance->client->getToken());
                    $customResponse = $requestWithCode->getDecodedResponse();

                    if (!is_array($customResponse)) {
                        ioHelper::line("Weird response from challenge validation...", App::DEBUG_LEVEL_ANY_DEBUG);
                        ioHelper::print_r($customResponse, App::DEBUG_LEVEL_ANY_DEBUG);
                        throw new \Exception('Weird response from challenge validation...', 1006);
                    }
                    /* $customResponse
                    Array
                    (
                        [action] => close
                        [status] => ok
                    )
                    */
                    if (
                        $customResponse['status'] === "ok"
                        && isset($customResponse['logged_in_user']['pk'])
                        && (string)$customResponse['logged_in_user']['pk'] === (string)$userId
                    ) {
                        ioHelper::message('Challenge accepted!', 5);
                        static::login($username, $password, $retry - 1);
                        return;
                    } else {
                        if (
                            $customResponse['status'] === "ok"
                            && isset($customResponse['action'])
                            && $customResponse['action'] === 'close'
                        ) {
                            ioHelper::message('Waiting confirmation in application', 1008);
                            throw new \Exception('Probably finished...', 1007);
                        } else {
                            throw new \Exception('Probably finished...', 1007);
                        }
                    }
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage(), 1005);
                }

            } else {
                throw new \Exception($e->getMessage(), 1004);
            }
        }
        // ioHelper::d(isset($loginResponse) ? $loginResponse : null);
        static::$loggedIn = true;
    }
    public static function logout() {
        if (!static::$loggedIn || APP::$isTest)
            return;
        $logoutResponse = static::$instance->logout();
        ioHelper::line('Logout...', App::DEBUG_LEVEL_ANY_DEBUG);
        # dump
        ioHelper::d($logoutResponse, App::DEBUG_LEVEL_ANY_DEBUG);
        static::$loggedIn = false;
    }
    /**
     * @param array $argv
     * @param array $shorts
     * @return array
     */
    public static function parseConsoleCommands(array $argv, $shorts = []) {
        if (!count($argv))
            return [];
        $globals = [];
        $defaultShorts = ['?' => 'help', 'help' => 'help'];
        $shorts[0] = array_merge(static::$shortCommands, $defaultShorts);
        $glob = 0; //$argv[0];
        $params = [];

        $fn_getScenarioShorts = function (string $scenarioName) use (&$defaultShorts): array {
            if (!in_array($scenarioName, static::$scenarioList))
                return [];
            $className = "roxblnfk\\instabot\\scenario\\{$scenarioName}";
            return array_merge($className::$shortCommands, $defaultShorts);
        };

        $name = null;
        for ($i = 1; $i < count($argv); ++$i) {
            $s = $argv[$i];
            if (substr($s, 0, 2) === '--') {
                $name = substr($s, 2);
            }
            # Short
            elseif (in_array(substr($s, 0, 1), ['-', '/'], true)) {
                $name = substr($s, 1);
                if (isset($shorts[$glob][$name]))
                    $name = $shorts[$glob][$name];
                else # short name not found
                    $name = $s;
            }
            # value
            else {
                if (!is_null($name)) {
                    if ($params[$name] === true)
                        $params[$name] = $s;
                    elseif (is_array($params[$name]))
                        $params[$name][] = $s;
                    else
                        $params[$name] .= " {$s}";
                } else { /* value without key!? */}
                continue;
            }
            # scenario
            if ($name === 'scenario') {
                $globals[$glob] = $params;
                $params = [];
                ++$i;
                $glob = isset($argv[$i]) ? $argv[$i] : null;
                if ($glob)
                    $shorts[$glob] = $fn_getScenarioShorts($glob);
            }
            # array
            elseif (isset($params[$name]) && $params[$name] !== true) {
                $params[$name] = (array)$params[$name];
            }
            # flag
            else {
                $params[$name] = true;
            }
        }

        if (is_scalar($glob))
            $globals[$glob] = $params;
        return $globals;
    }
    /**
     * Set debug level
     * @param int $value
     */
    public static function setDebug($value = 0) {
        ioHelper::$debugLevel = $value;
        if ($value > 0) {
            // if (static::$instance && $value >= static::DEBUG_LEVEL_ANY_DEBUG)
            //     static::$instance->debug = true;
            self::$isDebug = true;
            error_reporting(E_ALL | E_STRICT);
            ini_set('display_errors', 1);
            // Kint::$enabled_mode = true; #Kint v2
            Kint::enabled(true);
        } else {
            // if (static::$instance)
            //     static::$instance->debug = false;
            self::$isDebug = false;
            error_reporting(0);
            ini_set('display_errors', 0);
            // Kint::$enabled_mode = false; #Kint v2
            Kint::enabled(false);
        }
        ioHelper::line("Set debug mode to {$value}", self::DEBUG_LEVEL_MESSAGES);
    }

    /**
     * @param       $scenarioName
     * @param array $params
     * @return baseScenario
     * @throws \Exception
     */
    public static function loadScenario($scenarioName, $params) {
        $scenarioName = preg_replace('/[^a-z0-9_]+/ui', '', $scenarioName);
        ioHelper::line("Loading scenario {$scenarioName}", self::DEBUG_LEVEL_ALL);
        $className = "roxblnfk\\instabot\\scenario\\{$scenarioName}";
        if (!class_exists($className, true) && $className !== baseScenario::class)
            throw new \Exception('Scenario not found');

        ioHelper::line("Init scenario", self::DEBUG_LEVEL_ALL);
        /** @var baseScenario $scenario */
        $scenario = new $className(static::$instance, $params);

        return $scenario;
    }

    public static function exit() {

        if (!static::$modeHelp)
            ioHelper::message("Exit", App::DEBUG_LEVEL_EXCEPTION);
        die;
    }
    /**
     * Print HELP
     * @param array $scenarios
     */
    public static function help(array $scenarios = []) {
        static::$modeHelp = true;
        ioHelper::line();
        ioHelper::line('   run  -l <LOGIN> -p <PASSWORD> [-t <bool>] [-d <int>] [-?]');
        ioHelper::line('       [-s <SCENARIO> [<scenario options>]] [+ ...]');
        ioHelper::line();
        ioHelper::line('Params');
        ioHelper::line('    -t --test        Test mode (0 - off, 1 - on)');
        ioHelper::line('    -d --debug       Debug mode (0 - silent, 1...5 - more messages)');
        ioHelper::line('    -log --log-level Log level (will be written to the "log.log" file)');
        ioHelper::line('    -l --login       User\'s login');
        ioHelper::line('    -p --password    User\'s password');
        ioHelper::line('    -? --help        Help');
        ioHelper::line();
        ioHelper::line();
        if (!$scenarios) {
            ioHelper::line('Help with scenario options:');
            ioHelper::line('    -? -s <scenario> [-s <scenario>]');
            ioHelper::line();
            ioHelper::line('List of scenarios');
            foreach (static::$scenarioList as $scenarioName) {
                ioHelper::line('    ' . $scenarioName);
            }

        } else {
            foreach ($scenarios as $scenarioName => $scenarioProps) {
                $scenarioProps['help'] = true;
                try {
                    $scenario = static::loadScenario($scenarioName, $scenarioProps);
                    $scenario->help();
                } catch (\Exception $e) {
                    ioHelper::ioErr($e, App::DEBUG_LEVEL_EXCEPTION, false);
                }
            }

        }


    }

}
