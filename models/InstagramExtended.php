<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 12.10.2018
 * Time: 12:53
 */

namespace roxblnfk\instabot\models;


class InstagramExtended extends \InstagramAPI\Instagram {


    /**
     * Set the active account for the class instance.
     *
     * We can call this multiple times to switch between multiple accounts.
     *
     * @param string $username Your Instagram username.
     * @param string $password Your Instagram password.
     *
     * @throws \InvalidArgumentException
     * @throws \InstagramAPI\Exception\InstagramException
     */
    public function changeUser($username, $password) {
        return $this->_setUser($username, $password);
    }
}