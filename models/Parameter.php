<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 19.06.2018
 * Time: 20:28
 */

namespace roxblnfk\instabot\models;


class Parameter {

    public $name = 'undefined';
    public $required = false;
    public $default;
    /** @var array|mixed */
    public $value;
    /** @var string Type of param: flt, int, str, str[], int[] */
    public $type = 'str';
    public $description;

    /**
     * Parameter constructor.
     * @param array ...$values
     * @throws \Exception
     */
    public function __construct($values) {
        $fn_set = function ($name) use (&$values) {
            if (key_exists($name, $values))
                $this->$name = $values[$name];
        };
        # Name
        $fn_set('name');
        $fn_set('description');
        # Type
        $fn_set('type');
        # Default value
        if ($this->isArray())
            $this->value = [];
        if (key_exists('default', $values))
            $this->value = $values['default'];
        # Value
        $fn_set('required');
        if (key_exists('value', $values)) {
            // if ($this->isArray())
            //     $this->add($values['value']);
            // else
                $this->setVal($values['value']);
        }
        # Required
        elseif ($this->required) {
            throw new \Exception("Param `{$this->name}` required");
        }

    }

    private function isArray() {
        return substr($this->type, -2) === '[]';
    }
    /**
     * @param $v mixed
     * @throws \Exception
     */
    public function setVal($v) {
        if ($this->isArray()) {
            $this->value = [];
            foreach ((array)$v as $k => $v)
                $this->add($v, $k);
        } else {
            $this->value = $this->filterValue($v);
        }
    }
    /**
     * @param      $v
     * @param null $k
     * @throws \Exception
     */
    public function add($v, $k = null) {
        if (!is_array($this->value))
            $this->value = (array)$this->value;
        if (is_scalar($k))
            $this->value[$k] = $this->filterValue($v);
        else
            $this->value[] = $this->filterValue($v);
    }
    /**
     * @param $v
     * @return int|string
     * @throws \Exception
     */
    private function filterValue($v) {
        $substr = substr($this->type, 0, 3);
        if (is_array($v))
            $v = $this->filterValue(array_pop($v));
        if ($substr === 'int')
            return (int)$v;
        if ($substr === 'str')
            return (string)$v;
        if ($substr === 'flt')
            return (float)$v;
        throw new \Exception("Bad value for the param `{$this->name}`");
    }
}
