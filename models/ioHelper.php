<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 19.06.2018
 * Time: 16:42
 */

namespace roxblnfk\instabot\models;


use Kint;

class ioHelper {
    public static $outCharset = 'utf8';
    public static $debugLevel = App::DEBUG_LEVEL_ALL;

    public static $logLevel = App::DEBUG_LEVEL_ALL;
    public static $logFiles  = ['log.log'];

    public static $stdIn  = STDIN;
    public static $stdOut = STDOUT;
    public static $stdErr = STDERR;

    /**
     * Вывести форматированную строку в вывод
     * @param      $str
     * @param int  $code
     * @param int  $level
     */
    public static function message($str, $code = null, $level = App::DEBUG_LEVEL_OFF) {
        static::ioWrite(mb_convert_encoding("*** Message ({$code}): {$str}\r\n", static::$outCharset, 'utf8'), $level);
    }

    /**
     * @param     $mixed
     * @param int $code
     * @param int $level
     */
    public static function messageJSON($mixed, $code = 10, $level = App::DEBUG_LEVEL_OFF) {
        $str = json_encode($mixed, JSON_PARTIAL_OUTPUT_ON_ERROR|JSON_INVALID_UTF8_IGNORE/*|JSON_PRETTY_PRINT */);
        static::ioWrite("*** Message ({$code}): {$str}\r\n", $level);
    }
    /**
     * Вывести строку в вывод
     * @param string $str
     * @param int    $debugLevel
     * @param bool   $rn
     */
    public static function line($str = '', $debugLevel = App::DEBUG_LEVEL_ALL, bool $rn = true) {
        $arr = (array)$str;
        $rnt = $rn ? "\r\n" : '';
        foreach ($arr as $str) {
            if (ConsoleMode) {
                static::ioWrite(mb_convert_encoding($str, static::$outCharset, 'utf8') . (mb_strlen($str) == 80 ? '' : $rnt), $debugLevel);
            } else {
                static::ioWrite($str . ($rn ? "\r\n<br />" : ''), $debugLevel);
            }
        }
    }
    /**
     * Дамп значения
     * @param array ...$values
     */
    public static function d(...$values) {
        if (ConsoleMode) {
            $str = Kint::dump(...$values);
            static::ioWrite(mb_convert_encoding($str, static::$outCharset, 'utf8'), App::DEBUG_LEVEL_ANY_DEBUG);
            // static::ioWrite(iconv('utf8', static::$outCharset, $str));
            // static::ioWrite($str);
        } else {
            static::ioWrite(Kint::dump(...$values), App::DEBUG_LEVEL_ANY_DEBUG);
        }
    }
    /**
     * @param     $data
     * @param int $level
     */
    public static function ioWrite($data, $level = App::DEBUG_LEVEL_ALL) {
        if ($level <= static::$logLevel)
            static::toFile($data);
        if ($level <= static::$debugLevel)
            fwrite(static::$stdOut, $data);
    }
    /**
     * @return string
     */
    public static function ioRead() {
        return trim(fgets(static::$stdIn));
    }
    /**
     * @param      $error \Exception|string
     * @param int  $level
     * @param null $stackTrace
     */
    public static function ioErr($error = null, $level = App::DEBUG_LEVEL_EXCEPTION, $stackTrace = null) {
        if ($error instanceof \Exception)
            $data = '*** Fatal error (' . $error->getCode() . '): ' . $error->getMessage() . "\r\n" . ($stackTrace ? ($error->getTraceAsString()) : '');
        elseif (is_scalar($error))
            $data = $error;
        else
            $data = Kint::dump($error);

        if ($level <= static::$logLevel)
            static::toFile($data);
        if ($level <= static::$debugLevel)
            fwrite(static::$stdErr, $data);
    }
    /**
     * Вывод с помощью функции print_r
     * @param     $var
     * @param int $debugLevel
     */
    public static function print_r($var, $debugLevel = App::DEBUG_LEVEL_ANY_DEBUG) {
        $str = print_r($var, true);
        static::ioWrite(mb_convert_encoding($str, static::$outCharset, 'utf8'), $debugLevel);
    }
    /**
     * Зыписать вывод в лог-файл
     * @param string $str
     * @param int    $logfile
     */
    public static function toFile($str = '', $logfile = 0) {
        $timeline = date('[Y-m-d H:i:s] ');
        $file = static::$logFiles[$logfile] ?? false;
        if (!$file)
            return;
        file_put_contents($file, "\r\n{$timeline} {$str}", FILE_APPEND);
    }
}
