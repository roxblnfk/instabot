<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 19.06.2018
 * Time: 15:59
 */

namespace roxblnfk\instabot\scenario;

use InstagramAPI\Instagram;
use roxblnfk\instabot\models\App;
use roxblnfk\instabot\models\ioHelper;
use roxblnfk\instabot\models\Parameter;


abstract class baseScenario {

    /** @var Instagram */
    public $instance;
    /** @var Parameter[] */
    public $params = [];
    /** @var string[] */
    public static $shortCommands = [];

    /**
     * baseScenario constructor.
     * @param Instagram|null $instance
     * @param mixed[]        $params
     * @throws \Exception
     */
    public function __construct(Instagram $instance = null, array $params = []) {
        if (isset($params['help']) && App::$modeHelp)
            return;
        $this->instance = $instance;
        $tmpParams = $this->params;
        $this->params = [];
        foreach ($tmpParams as $name => $param) {
            $param['name'] = $name;
            if (key_exists($name, $params))
                $param['value'] = $params[$name];
            $this->params[$name] = new Parameter($param);
        }
        unset($tmpParams);
    }

    public function run() {
        ioHelper::line("Running scenario", App::DEBUG_LEVEL_ALL);
    }

    public function help() {
        ioHelper::line('Params');
        foreach ($this->params as $name => $param) {
            $shorts = '';
            if (in_array($name, static::$shortCommands)) {
                $tmp = (array)array_search($name, static::$shortCommands, true);
                $shorts = '-' . implode(' -', $tmp) . ' ';
            }

            ioHelper::line('    ' . str_pad("{$shorts}--{$name}", 15, ' ', STR_PAD_RIGHT) . " {$param['description']}");
        }
    }
}
