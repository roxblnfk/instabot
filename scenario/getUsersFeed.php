<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 19.06.2018
 * Time: 15:59
 */

namespace roxblnfk\instabot\scenario;


use InstagramAPI\Media\Photo\InstagramPhoto;
use roxblnfk\instabot\models\ioHelper;
use roxblnfk\instabot\models\Parameter;

class getUsersFeed extends baseScenario {

    /** @var Parameter[] */
    public $params = [
        'userid'   => [
            'required'    => false,
            'default'     => null,
            'type'        => 'int',
            'description' => 'User id. If not set then will be self id',
        ],
        'path'     => [
            'required'    => false,
            'default'     => null,
            'type'        => 'str',
            'description' => 'Dir to save',
        ],
        'maxid'     => [
            'required'    => false,
            'default'     => null,
            'type'        => 'str',
            'description' => 'Next "maximum ID", used for pagination',
        ],
    ];

    /** @var InstagramPhoto[] */
    public $photoBuffer = [];
    /** @var array */
    public $metaBuffer = [];


    public static $shortCommands = [
        'p' => 'path',
        'uid' => 'userid',
    ];


    /** @inheritdoc */
    public function run() {
        parent::run();

        $maxId = $this->params['maxid']->value;

        $feedResp = $this->instance
            ->timeline
            ->getUserFeed(intval($this->params['userid']->value ?: $this->instance->account_id), $maxId);
        $feedItems = $feedResp->getItems();

        ioHelper::messageJSON($feedItems);

        $path = $this->params['path']->value;
        if (!$path)
            return;
        if (!is_dir($path))
            mkdir($path, 0777, true);
        foreach ($feedItems as $id => $item) {
            if (!$item->isImageVersions2())
                continue;
            $images = $item->getImageVersions2()->getCandidates();
            if (!$images)
                continue;
            $big = $images[0];
            foreach ($images as $img) {
                $big = $img->getWidth() > $big->getWidth() ? $img : $big;
            }
            ioHelper::line($big->getWidth() . 'x' . $big->getHeight() . ': ' . $big->getUrl());

            $response = $this->instance->request($big->getUrl())->getHttpResponse();
            file_put_contents("{$path}/{$id}.jpg", $response->getBody());
        }
    }

    /** @inheritdoc */
    public function help() {
        ioHelper::line('This scenario does downloading user\'s feed\'s photos');
        ioHelper::line();

        parent::help();
    }
}
