<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 1.11.2019
 * Time: 14:10
 */

namespace roxblnfk\instabot\scenario;

use roxblnfk\instabot\models\App;
use roxblnfk\instabot\models\ioHelper;
use roxblnfk\instabot\models\Parameter;

class searchByHashtag extends baseScenario {

    /** @var Parameter[] */
    public $params = [
        'hashtag'   => [
            'required'    => true,
            'default'     => null,
            'type'        => 'str',
            'description' => 'Hashtag string',
        ],
        'return' => [
            'required'    => false,
            'default'     => 'tags',
            'type'        => 'str',
            'description' => 'What is to return [tags|feed|story]',
        ],
        'maxid'     => [
            'required'    => false,
            'default'     => null,
            'type'        => 'str',
            'description' => 'Next "maximum ID", used for pagination',
        ],
    ];

    public static $shortCommands = [
        'ht' => 'hashtag',
        'r'  => 'return',
    ];


    /** @inheritdoc */
    public function run() {
        parent::run();

        $hashtag = str_replace('#', '', $this->params['hashtag']->value);
        $maxId = $this->params['maxid']->value;

        switch ($this->params['return']->value) {
            case 'tags':
                $searchResp = $this->instance
                    ->hashtag
                    ->search('#' . $hashtag);
                $result = $searchResp->getResults();

                ioHelper::messageJSON($result);
                return;
            case 'feed':
                $searchResp = $this->instance
                    ->hashtag
                    ->getFeed($hashtag, App::$instance->uuid, $maxId);
                $result = $searchResp->getItems();
                ioHelper::messageJSON($result);
                return;
            case 'story':
                $searchResp = $this->instance
                    ->hashtag
                    ->getStory($hashtag);
                $result = $searchResp->getStory();
                ioHelper::messageJSON($result);
                return;
        }
    }

    /** @inheritdoc */
    public function help() {
        ioHelper::line('This scenario does searching by hashtag');
        ioHelper::line();

        parent::help();
    }
}
