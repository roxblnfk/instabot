<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 31.10.2019
 * Time: 15:59
 */

namespace roxblnfk\instabot\scenario;

use roxblnfk\instabot\models\ioHelper;
use roxblnfk\instabot\models\Parameter;

class searchUser extends baseScenario {

    /** @var Parameter[] */
    public $params = [
        'username'   => [
            'required'    => true,
            'default'     => null,
            'type'        => 'str',
            'description' => 'User name for search',
        ],
    ];

    public static $shortCommands = [
        'n' => 'username',
    ];


    /** @inheritdoc */
    public function run() {
        parent::run();

        $feedResp = $this->instance
            ->people
            ->search($this->params['username']->value);
        $resultUsers = $feedResp->getUsers();

        ioHelper::messageJSON($resultUsers);
    }

    /** @inheritdoc */
    public function help() {
        ioHelper::line('This scenario does searching of users');
        ioHelper::line();

        parent::help();
    }
}
