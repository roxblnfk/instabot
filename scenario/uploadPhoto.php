<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 19.06.2018
 * Time: 15:59
 */

namespace roxblnfk\instabot\scenario;


use InstagramAPI\Media\Photo\InstagramPhoto;
use InstagramAPI\Response\Model\Location;
use roxblnfk\instabot\models\App;
use roxblnfk\instabot\models\ioHelper;
use roxblnfk\instabot\models\Parameter;

class uploadPhoto extends baseScenario {

    /** @var Parameter[] */
    public $params = [
        'photo' => [
            'required' => true,
            'type' => 'str[]',
            'description' => 'Path to the photo',
        ],
        'caption' => [
            'required' => false,
            'default' => '',
            'type' => 'str[]',
            'description' => 'Caption',
        ],
        // 'location' => [
        //     'required' => false,
        //     'default' => null,
        //     'type' => Location::class . '[]',
        //     'description' => 'Location object. Must be JSON',
        // ],
    ];

    /** @var InstagramPhoto[] */
    public $photoBuffer = [];
    /** @var array */
    public $metaBuffer = [];


    public static $shortCommands = [
        'p' => 'photo',
        'c' => 'caption',
    ];


    /** @inheritdoc */
    public function run() {
        parent::run();

        $this->photoBuffer = [];
        $this->metaBuffer = [];

        foreach ($this->params['photo']->value as $num => $filename) {
            $meta = [];
            $caption = $this->params['caption']->value[$num] ?? null;
            if ($caption)
                $meta['caption'] = $caption;

            ioHelper::line("To upload photo [{$num}]:", App::DEBUG_LEVEL_MESSAGES);
            ioHelper::line(" > Caption (" . mb_strlen($caption) . "): {$caption}", App::DEBUG_LEVEL_MESSAGES);
            ioHelper::line(" > Filename: {$filename}", App::DEBUG_LEVEL_MESSAGES, false);
            if (!is_file($filename)) {
                ioHelper::line("\r\n   ! File not found!", App::DEBUG_LEVEL_MESSAGES);
                continue;
            }
            if (!is_readable($filename)) {
                ioHelper::line("\r\n   ! File can't read!", App::DEBUG_LEVEL_MESSAGES);
                continue;
            }
            ioHelper::line(" ... ok", App::DEBUG_LEVEL_MESSAGES);

            try {
                $photo = new InstagramPhoto($filename);
                $this->photoBuffer[$num] = $photo;
                $this->metaBuffer[$num] = $meta;
                unset($photo);
            } catch (\Exception $e) {
                ioHelper::line("   ! File not loaded!", App::DEBUG_LEVEL_MESSAGES);
                ioHelper::ioErr($e, App::DEBUG_LEVEL_EXCEPTION);
                continue;
            }
        }
        foreach ($this->photoBuffer as $num => $photo) {

            ioHelper::line(" * Uploading photo [{$num}]...", App::DEBUG_LEVEL_MESSAGES);
            if (!App::$isTest) {
                try {
                    $file = $photo->getFile();
                    $res = $this->instance->timeline->uploadPhoto($file, $this->metaBuffer[$num]);
                    # dump
                    ioHelper::line(' ... Uploaded', App::DEBUG_LEVEL_ANY_DEBUG);
                    ioHelper::print_r($res, App::DEBUG_LEVEL_ANY_DEBUG);
                } catch (\Exception $e) {
                    ioHelper::d($e);
                    throw $e;
                }
            }
        }
    }

    /** @inheritdoc */
    public function help() {
        ioHelper::line('This scenario does publication of the photos');
        ioHelper::line();

        parent::help();
    }
}
