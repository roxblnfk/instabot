<?php
/**
 * Created by PhpStorm.
 * User: ROX
 * Date: 19.06.2018
 * Time: 15:59
 */

namespace roxblnfk\instabot\scenario;


use InstagramAPI\Media\Photo\InstagramPhoto;
use roxblnfk\instabot\models\App;
use roxblnfk\instabot\models\ioHelper;
use roxblnfk\instabot\models\Parameter;

class userAction extends baseScenario {

    /** @var Parameter[] */
    public $params = [
        'userid'   => [
            'required'    => true,
            'default'     => null,
            'type'        => 'int',
            'description' => 'User id',
        ],
        'action' => [
            'required'    => false,
            'default'     => 'info',
            'type'        => 'str',
            'description' => 'Action [unfollow|follow|info]',
        ],
    ];

    public static $shortCommands = [
        'a' => 'action',
        'uid' => 'userid',
    ];


    /** @inheritdoc */
    public function run() {
        parent::run();

        $userid = $this->params['userid']->value;

        switch ($this->params['action']->value) {
            case 'unfollow':
                $response = $this->instance
                    ->people
                    ->unfollow($userid);
                $result = $response->getFriendshipStatus();

                ioHelper::messageJSON($result);
                return;
            case 'follow':
                $response = $this->instance
                    ->people
                    ->follow($userid);
                $result = $response->getFriendshipStatus();

                ioHelper::messageJSON($result);
                return;
            case 'info':
            default:
                $response = $this->instance
                    ->people
                    ->getInfoById($userid);
                $result = $response->getUser();

                ioHelper::messageJSON($result);
                return;
        }
    }

    /** @inheritdoc */
    public function help() {
        ioHelper::line('This scenario does downloading user\'s feed\'s photos');
        ioHelper::line();

        parent::help();
    }
}
